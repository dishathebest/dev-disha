@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register Customer</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ isset($mode) ? action('CustomersController@update', $customer->id) : url('customer') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
<?php //print_r($errors->getMessages());die; ?>
                        @if(isset($mode))
                            <input type="hidden" name="_method" value="PATCH" />
                        @endif
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">First Name*</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ isset($mode) ? $customer->first_name : old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ isset($mode) ? $customer->last_name : old('last_name') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{  isset($mode) ? $customer->email : old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Phone*</label>
                            
                            <div class="col-md-6">
                                <div id="phone_section">
                                    @if(isset($mode) && count($customer->phone) > 0)
                                        @foreach($customer->phone as $phone)
                                        <input type="text" class="form-control" name="phone[]" value="{{ $phone->phone_number }}" required>
                                        @endforeach
                                    @else
                                        <input type="text" class="form-control" name="phone[]" value="{{ old('phone') }}" required>
                                    @endif
                                </div>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                <a href="javascript:void(0);" class="add_more_phone">Add more</a>
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Details</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="details">{{ isset($mode) ? $customer->details : old('details') }}</textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Image</label>

                            <div class="col-md-6">
                                <input type="file" name="image1" />
                                @if ($errors->has('image1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image1') }}</strong>
                                    </span>
                                @endif
                                <br />
                                @if(isset($mode) && $customer->image!='')
                                    <img src="{{ asset('public/uploads/'.$customer->image) }}" width="100" height="100" />
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ isset($mode) ? "Update" : "Register" }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection