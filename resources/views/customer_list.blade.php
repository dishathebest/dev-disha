@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Customer List</div>

                <div class="panel-body">
                    <table border="0" width="100%">
                        <tr>
                            <th>Customer</th>
                            <th>Added By</th>
                            <th>Action</th>
                        </tr>
                        @foreach($customers as $cust)
                        <tr>
                            <td>{{ $cust->first_name }}</td>
                            <td>{{ $cust->employee->name }}</td>
                            <td>
                                <a href="{{ action('CustomersController@edit', $cust->id) }}" class="btn btn-primary">Edit</a>
                                <form action="{{action('CustomersController@destroy', $cust->id)}}" method="post">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <?php echo $customers->render(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection