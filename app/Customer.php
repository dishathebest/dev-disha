<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $table = 'customers';
    
    //protected $dates = ['deleted_at'];
    
    
    public function phone(){
        return $this->hasMany('App\Phone','customer_id',"id");
    }
    
    public function employee(){
        return $this->belongsTo('App\User','emp_id','id');
    }
}
