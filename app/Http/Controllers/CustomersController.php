<?php

namespace App\Http\Controllers;

use App\Customers;
use Illuminate\Http\Request;
use Validator;
use App\Customer;
use App\Phone;
use Auth;
use File;
use Illuminate\Pagination\Paginator;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::with('employee')->paginate(5);
        return view("customer_list",array("customers"=>$customers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("customer_create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            "first_name" => 'required|max:190',
            "email" => 'nullable|email',
            'phone.*' =>'required',
            //"image1" => 'mimes:jpg,gif,png'
        ]);
        
        if($validator->fails()){
            return redirect("customer/create")->withInput()->withErrors($validator);
        }
        
        $path = public_path("uploads");
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        
        $image_name = "";
        if($request->hasFile("image1")) {
            $image_name = time().".".$request->image1->getClientOriginalExtension();
            $request->image1->move($path,$image_name);
        }
        
        $customers = new Customer;
        $customers->emp_id = Auth::id();
        $customers->first_name = $request->first_name;
        $customers->last_name = $request->last_name;
        $customers->email = $request->email;
        $customers->details = $request->details;
        $customers->image = $image_name;
        $customers->save();
        
        // make phone entry
        $phoneArr = array();
        foreach($request->phone as $phones) {
            $phoneArr[]=new Phone(['phone_number' => $phones]);
        }
        $customers->phone()->saveMany($phoneArr);
        
        //send email
        $name = $request->first_name;
        $email = $request->email;
        if($request->email != '') {
            Mail::send('email.welcome_cust', ['name' => $name], function ($message) use($name, $email)
            {
                $message->to($email, $name)->subject('Welcome to AlienTechsol!');

            });
        }        
        
        return redirect("customer");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customers, $id)
    {
        $customer = Customer::where("id",$id)->with('phone')->first();
        return view("customer_create",array("customer"=>$customer,"mode"=>"edit"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customers, $id)
    {
        $validator = Validator::make($request->all(),[
            "first_name" => 'required|max:190',
            "email" => "nullable|email",
            'phone.*' =>'required',
            "image" => 'nullable|image|mimes:jpg,jpeg,gif,png'
        ]);
        
        if($validator->fails()) {
            return redirect("customer/create")->withInput()->WithErrors($validator);
        }
        
        $image_name = "";
        if($request->hasFile("image1")) {
            $image_name = time().".".$request->image1->getClientOriginalExtension();
            $request->image1->move(public_path("uploads"),$image_name);
        }
        
        $customers = Customer::find($id);
        $customers->first_name = $request->first_name;
        $customers->last_name = $request->last_name;
        $customers->email = $request->email;
        $customers->details = $request->details;
        if($image_name!=''){
            $customers->image = $image_name;
        }        
        $customers->save();
        
        $customers->phone()->delete();
        $phoneArr=array();
        foreach($request->phone as $phone) {
            $phoneArr[] = new Phone(['phone_number' => $phone]);
        }
        $customers->phone()->saveMany($phoneArr);
        return redirect("customer");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customers, $id)
    {
        $customer = Customer::find($id)->delete();
        return redirect("customer");
    }
}
